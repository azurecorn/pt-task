## Task - Validate And Store Addresses ##

Validate, Insert and List User Addresses
 
## Install note ##

*   run: composer install
*   setup host, file permissions for the project
*   import DB_structure.sql
*   insert DB credentials in .env file, your API data

### Server Requirements ###
*   PHP 7.4
*   MySql 5.7 and above

### Technologies ###
*   PHP
*   MySql
*   Twitter Bootstrap
*   Jquery
*   composer