<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row mt-4">
            <div class="col offset-lg-2 col-lg-8">
                <h1>Address App</h1>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col offset-lg-2 col-lg-8">
                <form action="" id="add-form" method="POST">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" name="last_name"  id="last_name"placeholder="Enter Last Name">
                    </div>
                    <div class="form-group">
                        <label for="street">Street/Number</label>
                        <input type="text" class="form-control" name="street" id="street" placeholder="Enter Street/Number">
                    </div>
                    <div class="form-group">
                        <label for="postal">Postal Code</label>
                        <input type="text" class="form-control" name="postal" id="postal" placeholder="Enter Postal Code">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" name="city" id="city" placeholder="Enter City">
                    </div>
                    <div class="form-group has-danger">
                        <label for="city">Country</label>
                        <input type="text" class="form-control" name="country" id="country" placeholder="Country" readonly value="Germany">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="row mt-4 mb-4">
            <div class="col offset-lg-2 col-lg-8">
                <h5><a href="/?list">Visit a List</a> of Previously Validated and Stored Addresses.</h5>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {

            $("form").submit(function(e){
                e.preventDefault();

                //if there was a previous error, remove red border
                $('input').removeClass('is-invalid');

                $.ajax({
                    url: 'validate-and-store.php',
                    method: 'post',
                    data:  $("form").serialize(),
                    encode: true,
                    success: function(data, textStatus, jqXHR){
                        alert('Address Validated and Stored');
                        window.location.reload();
                    },
                    error: function (xhr, status, errorThrown) {
                        let response = jQuery.parseJSON(xhr.responseText);

                        $.each( response, function( key, value ) {
                            $('#' + key).addClass('is-invalid');
                        });
                    }
                })
            });
        });
    </script>
</body>
</html>