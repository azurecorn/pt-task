<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
<div class="container-fluid">
    <div class="row mt-4">
        <div class="col offset-lg-2 col-lg-8">
            <h1>Address List</h1>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col offset-lg-2 col-lg-8">
            <?php if(isset($addresses)):?>
            <?php foreach($addresses as $address):?>
                <div class="row mb-4">
                    <div class="col-lg-6 bg-success">
                        ID:
                    </div>
                    <div class="col-lg-6 bg-success">
                        <?php echo $address->id?>
                    </div>
                    <div class="col-lg-6">
                        First name:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->first_name)?>
                    </div>
                    <div class="col-lg-6">
                        Last name:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->last_name)?>
                    </div>
                    <div class="col-lg-6">
                        Street:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->street)?>
                    </div>
                    <div class="col-lg-6">
                        Postal Code:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->postal)?>
                    </div>
                    <div class="col-lg-6">
                        City:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->city)?>
                    </div>
                    <div class="col-lg-6">
                        Country:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->country)?>
                    </div>
                    <div class="col-lg-6">
                        API validated quality:
                    </div>
                    <div class="col-lg-6">
                        <?php echo htmlspecialchars($address->api_validated_quality)?>
                    </div>
                    <div class="col-lg-6">
                        API vadidated at:
                    </div>
                    <div class="col-lg-6">
                        <?php echo $address->api_validated_at?>
                    </div>
                </div>
            <?php endforeach?>
            <?php endif?>
        </div>
    </div>
    <div class="row mt-4 mb-4">
        <div class="col offset-lg-2 col-lg-8">
            <h5><a href="/">Visit Homepage</a> and add a new address</h5>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>