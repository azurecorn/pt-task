<?php

// Autoload classes
require "vendor/autoload.php";

// Load .env values
(Dotenv\Dotenv::createImmutable(__DIR__))->load();

/**
 * As an entry point to the app, this file acts like a very simple router.
 * If it's home page, loads template for address adding and validation page,
 * else loads data and template for address listing page
 */
if (!isset($_GET['list'])) {
    include 'templates/add.php';
} else {
    $pdo = new \App\DBadapter\PDOadapter();
    $address = new \App\Model\Address($pdo);
    $addresses = $address->get();

    include 'templates/list.php';
}
