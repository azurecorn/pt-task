<?php

declare(strict_types=1);

namespace App\Model;

use App\DBadapter\PDOadapter;
use PDO;

/**
 * Class Address is our app main entity. Currently this class mainly holds a logic for DB operations.
 */
class Address
{
    private $conn;

    /**
     * Address constructor.
     * @param PDOadapter $dbAdapter
     */
    public function __construct(PDOadapter $dbAdapter)
    {
        $this->conn = $dbAdapter->getConnection();
    }

    /**
     * Saving a new address
     *
     * @param array $addressData
     * @param \stdClass $responseFromExternalApi
     */
    public function save(array $addressData, \stdClass $responseFromExternalApi): void
    {
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "
                INSERT INTO addresses (first_name,last_name,street,postal,city,country,api_validated_quality,api_validated_at)
                VALUES ( :first_name, :last_name, :street, :postal, :city, :country, :api_validated_quality, :api_validated_at);
            ";

            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue("first_name", $addressData['first_name'], PDO::PARAM_STR);
            $stmt->bindValue("last_name", $addressData['last_name'], PDO::PARAM_STR);
            $stmt->bindValue("street", $addressData['street'], PDO::PARAM_STR);
            $stmt->bindValue("postal", $addressData['postal'], PDO::PARAM_STR);
            $stmt->bindValue("city", $addressData['city'], PDO::PARAM_STR);
            $stmt->bindValue("country", $addressData['country'], PDO::PARAM_STR);
            $stmt->bindValue("api_validated_quality", $responseFromExternalApi->result[0]->quality, PDO::PARAM_INT);
            $stmt->bindValue("api_validated_at",
                $this->fixTimeFormatFromServer($responseFromExternalApi->timestamp), PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->conn = null;
    }

    /**
     * Returns address entries
     *
     * @return array|null
     */
    public function get(): ?array
    {
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT id,
                            first_name,
                            last_name,
                            street,
                            postal,
                            city,
                            country,
                            api_validated_quality,
                            api_validated_at
                    FROM addresses
                    ORDER BY id DESC;";

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $addresses = $stmt->fetchAll(PDO::FETCH_OBJ);

            $this->conn = null;

            return $addresses;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Timestamp param from server comes in format 2020-05-09T23:15:19+00:00 and this function fixes it for app db
     *
     * @param $serverTime
     * @return string
     */
    private function fixTimeFormatFromServer($serverTime): string
    {
        return str_replace(array('T', '+00:00'), array(' ', ''), $serverTime);
    }
}