<?php

declare(strict_types=1);

namespace App\Validator;

use App\Http\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class External
 * @package App\Validator
 */
class External
{
    private array $formData;
    private string $url;
    private Client $httpClient;

    public function __construct(array $formData, Client $httpClient)
    {

        $this->httpClient = $httpClient;
        $this->formData = $formData;

        $this->url = getenv('PT_API_URL');
    }

    /**
     * Call for external API validation service(PT API)
     *
     * @return mixed
     */
    public function addressCheck()
    {
        try {
            $request = $this->httpClient->instance->request('GET', $this->url, [
                'query' => [
                    'token' => getenv('API_KEY'),
                    'street' => htmlspecialchars($this->formData['street']),
                    'postal' => htmlspecialchars($this->formData['postal']),
                    'city' => htmlspecialchars($this->formData['city']),
                    'country' => htmlspecialchars($this->formData['country']),
                ]
            ]);

            $result = json_decode($request->getBody()->read(1024));

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $errorResponse = $e->getResponse()->getBody()->read(1024);
                if (strpos($errorResponse, 'NULL') !== false) {
                    $errorResponse = substr($errorResponse, 0, -3);
                }
                $result = json_decode($errorResponse);
            }
        }

        return $result;

    }
}