<?php

declare(strict_types=1);

namespace App\Validator;

/**
 * Class Local
 * @package App\Validator
 */
class Local
{
    private array $formData;

    /**
     * Local constructor.
     * @param array $formData
     */
    public function __construct(array $formData)
    {
        $this->formData = $formData;
    }

    /**
     * Check if user first and last name contain special characters or is empty. If so, returns an array of an errors with keys.
     *
     * @return array|bool
     */
    public function containsSpecialCharacters()
    {
        foreach ($this->formData as $key => $value) {
            $invalid = $this->hasInvalidValue($value);
            if ($invalid) {
                return [$key => 'Inputs Should Not Contain Special Characters Nor to Be Empty'];
            }
        }

        return false;
    }

    /**
     * In a case of special characters or blank value, this function returns 1, else 0
     *
     * @param string $userInput
     * @return int
     */
    private function hasInvalidValue(string $userInput): int
    {
        if ($userInput) {
            return preg_match('/[$&+,:;=?@#|\<>^*%!]/', $userInput);
        }

        return 1;
    }
}