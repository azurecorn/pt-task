<?php

declare(strict_types=1);

namespace App\Http;

/**
 * Class Response
 * @package App\Http
 */
class Response implements ResponseInterface
{
    /**
     * Adds HTTP response code and packs a body for a response for frontend
     *
     * @param int $status
     * @param null $body
     * @return mixed|void
     */
    public function send($status = 200, $body = null)
    {
        //set response HTTP status
        http_response_code($status);

        //prepare response content and return
        echo json_encode($body);
    }
}