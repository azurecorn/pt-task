<?php

declare(strict_types=1);

namespace App\Http;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Class Client holds the HTTP client. Currently GuzzleClient is used.
 * @package App\Http
 */
class Client
{
    public GuzzleClient $instance;

    public function __construct()
    {
        $this->instance = new GuzzleClient();
    }
}