<?php

// Autoload classes
require "vendor/autoload.php";

// Load .env values
(Dotenv\Dotenv::createImmutable(__DIR__))->load();

/**
 * AJAX from address adding page
 */
if ($_POST) {
    $httpClient = new App\Http\Client();
    $response = new App\Http\Response();

    $onlyNameParametersFromRequest = [
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name']
    ];

    //special characters&blank check for first name and last name
    $localValidator = new App\Validator\Local($onlyNameParametersFromRequest);
    $containsSpecialCharacters = $localValidator->containsSpecialCharacters();

    /**
     * If there are special chars, this code below is returning HTTP 400 to front and a key of failing input.
     * If there are errors gotten from external API validation service, also returns 400 and keys.
     * If there are no errors, store user address.
     */
    if ($containsSpecialCharacters) {
        $status = \App\Http\ResponseInterface::HTTP_BAD_REQUEST;
        $response->send($status, $containsSpecialCharacters);

    } else {
        //external api check
        $externalValidator = new App\Validator\External($_POST, $httpClient);
        $externalValidatorResult = $externalValidator->addressCheck();

        if (isset($externalValidatorResult->error)) {
            $status = \App\Http\ResponseInterface::HTTP_BAD_REQUEST;
            $response->send($status, $externalValidatorResult->error);

        } else {
            $pdo = new \App\DBadapter\PDOadapter();
            $address = new \App\Model\Address($pdo);
            $address->save($_POST, $externalValidatorResult);

            $status = \App\Http\ResponseInterface::HTTP_OK;
            $response->send($status, ['sehr gut']);
        }
    }
}
